package com.skrumble.android_sdk.enumeration;

public enum Role {

    Admin, Member, Guest, Invalid;

    public static Role getRole(String role) {
        switch (role) {
            case "admin":
                return Admin;
            case "member":
                return Member;
            case "guest":
                return Guest;
            default:
                return Invalid;
        }
    }
}
