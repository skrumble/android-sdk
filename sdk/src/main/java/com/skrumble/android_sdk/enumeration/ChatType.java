package com.skrumble.android_sdk.enumeration;

public enum ChatType {

    Room("room"), Private("private"), Department("department");

    private String description;

    ChatType(String s) {
        description = s;
    }

    public static ChatType fromString(String s) {
        switch (s) {
            case "department":
                return Department;
            case "room":
                return Room;
            default:
                return Private;

        }
    }

    public String getDescription() {
        return description;
    }
}