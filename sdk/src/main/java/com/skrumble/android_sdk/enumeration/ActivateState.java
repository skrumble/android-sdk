package com.skrumble.android_sdk.enumeration;

public enum ActivateState {

    Pending, PayWall, Activated, Invalid;

    public static ActivateState getState(String state) {
        switch (state) {
            case "pending":
                return Pending;
            case "paywall":
                return PayWall;
            case "activated":
                return Activated;
            default:
                return Invalid;
        }
    }
}
