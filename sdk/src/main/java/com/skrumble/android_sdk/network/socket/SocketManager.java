package com.skrumble.android_sdk.network.socket;


import android.os.AsyncTask;
import android.os.Bundle;

import com.skrumble.android_sdk.Config;
import com.skrumble.android_sdk.helper.JsonHelper;
import com.skrumble.android_sdk.network.Networking;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.OkHttpClient;

public class SocketManager {
    public static final String TAG = SocketManager.class.getSimpleName();

    private static SocketManager sInstance;

    private Socket socket;
    private List<AbstractSocketListener> mObservers = new ArrayList<>();
    private ReconnectAsyncTask mReconnectAsyncTask = new ReconnectAsyncTask();

    // *********************************************************************************************
    // region Constructor

    private SocketManager() {
        mObservers = new ArrayList<>();
        createSocketIOInstance();
    }

    public static SocketManager getInstance() {
        if (sInstance == null) {
            sInstance = new SocketManager();
        }

        return sInstance;
    }

    // endregion

    // *********************************************************************************************
    // region Create

    private void createSocketIOInstance() {
        if (socket != null) {
            return; // already created
        }
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

        } catch (Exception e) {
        }

        OkHttpClient okHttpClient = builder.build();

        IO.Options options = new IO.Options();
        options.secure = true;
        options.callFactory = okHttpClient;
        options.webSocketFactory = okHttpClient;
        // No need to worry about port
        // it is dynamic on server side
        // options.port = NetworkConfig.SOCKET_PORT;
        options.transports = new String[]{"websocket"};
        try {
            socket = IO.socket(Config.getSocketServer(), options);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            socket = null;
        }
    }

    // endregion

    // *********************************************************************************************
    // region Utility

    public boolean isConnected() {
        boolean isConnected = false;

        if (socket != null) {
            isConnected = socket.connected();
        }
        return isConnected;
    }

    // endregion

    // *********************************************************************************************
    // region Connect Disconnect

    public void connect() {
        // instanciate here
        createSocketIOInstance();

        if (socket == null) {
            return;
        }

        if (socket.connected()) {
            return; // already connected
        }

        socket.on(Socket.EVENT_CONNECT, onConnect)
                .on(Socket.EVENT_CONNECT_TIMEOUT, onConnectTimeout)
                .on(Socket.EVENT_CONNECT_ERROR, onConnectError)
                .on(Socket.EVENT_ERROR, onEventError);

        if (mObservers.size() == 0) {
            registerForAllObserver();
        }

        for (AbstractSocketListener observer : mObservers) {
            observer.onSubscribeToSocketEvents(socket);
        }

        socket.connect();
    }

    public void disconnect() {
        // socket already disconnected
        if (socket == null) {
            return;
        }

        socket.on(Socket.EVENT_DISCONNECT, onDisonnect);

        socket.disconnect();

        socket.off(Socket.EVENT_CONNECT, onConnect)
                .off(Socket.EVENT_DISCONNECT, onDisonnect)
                .off(Socket.EVENT_CONNECT_TIMEOUT, onConnectTimeout)
                .off(Socket.EVENT_CONNECT_ERROR, onConnectError)
                .off(Socket.EVENT_ERROR, onEventError);

        for (AbstractSocketListener observer : mObservers) {
            observer.onUnsubscribeToSocketEvents(socket);
        }
    }

    // endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // region OBSERVER REGISTRATION

    public void registerForAllObserver() {

    }

    public synchronized boolean registerObserver(AbstractSocketListener observer) {
        if (observer == null) {
            return false;
        }
        return mObservers.add(observer);
    }

    public synchronized boolean unregisterObserver(AbstractSocketListener observer) {
        if (observer == null) {
            return false;
        }

        return mObservers.remove(observer);
    }

    public synchronized void removeAllObservers() {
        for (AbstractSocketListener observer : mObservers) {
            // We are using Event Bus for that
            // If we need to have specific individual listener,
            // let implements it in the abstract socket lister

        }

        mObservers.clear();
    }

    // endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // region SOCKET EVENT

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (Config.getAuthString() != null || Config.getAuthString().isEmpty() == false){
                registerSocket();
            }
        }
    };

    private Emitter.Listener onDisonnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            unregisterSocket();
        }
    };

    private Emitter.Listener onConnectTimeout = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (mReconnectAsyncTask.isCancelled()) {
                mReconnectAsyncTask.execute();
            }
        }
    };

    private Emitter.Listener onEventError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }
    };

    public void registerSocket(){
        sendSocketRequest(Networking.EVENT_POST, "/socket/register", null, new SimpleApiCallBack<Object>() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                if (success) {

                } else {

                }
            }
        });
    }

    public void unregisterSocket(){
        sendSocketRequest(Networking.EVENT_POST, "/socket/unregister", null, new SimpleApiCallBack<Object>() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                if (success) {

                } else {

                }
            }
        });
    }

    // endregion

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // region REQUEST

    public void sendSocketRequest(String method, String api, HashMap<String, Object> dataMap, final SimpleApiCallBack<Object> callBack) {
        final JSONObject request = createSocketRequest(method, api, dataMap);
        if (socket == null) {
            return;
        }
        socket.emit(method.toLowerCase(), request, new Ack() {
            @Override
            public void call(Object... args) {
                if (callBack == null) {
                    return; // no need to fetch object
                }
                JSONObject response = getJSONObjectFromServerResponse(args);
                Object body = JsonHelper.getJsonObjectOrJsonArray(response, "body");

                int statusCode = JsonHelper.getIntFromJSON(response, "statusCode");
                boolean success = Networking.isSuccessResponse(statusCode);
                callBack.onApiResponse(body, success, statusCode);
            }
        });
    }

    private JSONObject createSocketRequest(String method, String url, HashMap<String, Object> data) {
        JSONObject request = new JSONObject();

        // HEADER
        JSONObject headers = new JSONObject();

        if (url.contains(Config.getApiServer())){
            url = url.replace(Config.getApiServer(), "");
        }

        if (Config.getApiServer().contains("/v3")) {
            url = "/v3" + url;
        }

        try {
            headers.put("Authorization", Config.getAuthString());

            request.put("method", method.toLowerCase());
            request.put("url", url);
            request.put("headers", headers);

            // DATA
            if (data != null) {
                JSONObject dataObj = new JSONObject(data);
                request.put("data", dataObj);
            }

        }catch (Exception e){

        }

        return request;
    }

    private JSONObject getJSONObjectFromServerResponse(Object... args) {
        int index = args.length - 1;
        if (index < 0) {
            return null;
        }
        return (JSONObject) args[index];
    }

    // endregion

    class ReconnectAsyncTask extends AsyncTask<Bundle, Void, Void> {
        @Override
        protected Void doInBackground(Bundle... bundles) {
            try {
                Thread.sleep(Config.getSocketReconnectInterval());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mReconnectAsyncTask.cancel(true);
            connect();
        }
    }
}
