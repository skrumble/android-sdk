package com.skrumble.android_sdk.network;

import com.skrumble.android_sdk.model.Chat;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatNetworking extends Networking {

    private static String getChatPopulateParameters() {
        return "populate=users,last_message&unread=true";
    }

    private static String getChatListUrl() {
        String populateString = getChatPopulateParameters();
        return getApiServerBaseUrl("/chat?" + populateString + "&limit=1000&skip=0&favorite=true");
    }

    private static String getSingleChatUrl(String chatID){
        String populateString = getChatPopulateParameters();
        return getApiServerBaseUrl("/chat/" + chatID + "/" + populateString);
    }

    private static String getCreateChatUrl(){
        return getApiServerBaseUrl("/chat");
    }

    private static String getEditChatUrl(String chatID){
        return getApiServerBaseUrl("/chat/" + chatID);
    }

    private static String getDeleteChatUrl(String chatID){
        return getApiServerBaseUrl("/chat/" + chatID);
    }

    // *********************************************************************************************
    // region Get Chat

    public static void getAllChat(final SimpleApiCallBack<ArrayList<Chat>> callBack){


        sendGetRequest(getChatListUrl(), new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                ArrayList<Chat> chats = new ArrayList<>();

                if (success){
                    chats = Chat.chatArrayListFactory((JSONArray) result);
                }

                if (callBack != null){
                    callBack.onApiResponse(chats,success, statusCode);
                }
            }
        });
    }

    public static void getSingleChat(String chatID, final SimpleApiCallBack<Chat> callBack){

        sendGetRequest(getSingleChatUrl(chatID), new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                Chat chat = null;
                if (success){
                    chat = new Chat((JSONObject) result);
                }

                if (callBack != null){
                    callBack.onApiResponse(chat, success, statusCode);
                }
            }
        });
    }

    // endregion

    // *********************************************************************************************
    // region Create - Update

    public static void createChat(String chatName, String purpose, String chatType, ArrayList<String> userIDList, final SimpleApiCallBack<Chat> callBack){

        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("name", chatName);
        dataMap.put("type", chatType);
        dataMap.put("purpose", purpose);
        dataMap.put("users", userIDList);

        sendPostRequest(getCreateChatUrl(), dataMap, new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                Chat chat = null;

                if (success){
                    chat = new Chat((JSONObject) result);
                }


                if (callBack != null){
                    callBack.onApiResponse(chat, success, statusCode);
                }
            }
        });

    }

    public static void editChat(String chatID, String chatName, String purpose, final SimpleApiCallBack callBack){

        HashMap<String, Object> map = new HashMap<>();
        map.put("name", chatName);
        map.put("purpose", purpose);

        sendPatchRequest(getEditChatUrl(chatID), map, new SimpleApiCallBack() {
            @Override
            public void onApiResponse(Object result, Boolean success, int statusCode) {
                if (callBack != null){
                    callBack.onApiResponse(result, success, statusCode);
                }
            }
        });
    }

    // endregion

    // *********************************************************************************************
    // region Delete

    public static void delete(String chatID, final SimpleApiCallBack callBack){
        final String api = getDeleteChatUrl(chatID);
        sendDeleteRequest(api, callBack);
    }

    // endregion
}
