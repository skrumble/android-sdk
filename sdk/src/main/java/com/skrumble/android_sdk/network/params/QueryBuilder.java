package com.skrumble.android_sdk.network.params;

import android.net.Uri;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

public class QueryBuilder {
    List<String> mQuery = new ArrayList<>();

    public QueryBuilder add(String k, String v) {
        if (!Strings.isNullOrEmpty(k) && !Strings.isNullOrEmpty(v)) {
            mQuery.add(String.format("%s=%s", k, Uri.encode(v)));
        }

        return this;
    }

    public QueryBuilder add(String k, int v) {
        final String vString = String.valueOf(v);
        mQuery.add(String.format("%s=%s", k, Uri.encode(vString)));
        return this;
    }

    public QueryBuilder add(String k, boolean v) {
        final String vString = String.valueOf(v);
        mQuery.add(String.format("%s=%s", k, Uri.encode(vString)));
        return this;
    }

    public QueryBuilder add(String k, List<String> v) {
        if (!Strings.isNullOrEmpty(k) && v != null && v.size() > 0) {
            mQuery.add(String.format("%s=%s", k, Joiner.on(",").join(v)));
        }

        return this;
    }

    public QueryBuilder add(String k, String[] v) {
        if (!Strings.isNullOrEmpty(k) && v != null && v.length > 0) {
            mQuery.add(String.format("%s=%s", k, Joiner.on(",").join(v)));
        }

        return this;
    }

    public String build() {
        return Joiner.on("&").join(mQuery);
    }
}
