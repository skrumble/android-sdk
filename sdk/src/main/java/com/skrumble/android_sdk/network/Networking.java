package com.skrumble.android_sdk.network;

import android.os.Handler;
import android.os.Looper;

import com.skrumble.android_sdk.Config;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;
import com.skrumble.android_sdk.network.socket.SocketManager;

import java.util.HashMap;


public class Networking {
    private static final String EVENT_GET = "get";
    public static final String EVENT_POST = "post";
    private static final String EVENT_PATCH = "patch";
    private static final String EVENT_DELETE = "delete";


    static String getAuthServerBaseUrl(String api, Object... args) {
        return String.format("%s%s", Config.getAuthServer(), String.format(api, args));
    }

    static String getApiServerBaseUrl(String api, Object... args) {
        return String.format("%s%s", Config.getApiServer(), String.format(api, args));
    }

    public static boolean isSuccessResponse(int status) {
        return status >= 200 && status < 300;
    }

    static void sendPatchRequest(final String url, HashMap<String, Object> dataMap, SimpleApiCallBack callBack) {
        sendApiRequest(EVENT_PATCH, url, dataMap, callBack);
    }

    static void sendPostRequest(final String url, HashMap<String, Object> dataMap, SimpleApiCallBack callBack) {
        sendApiRequest(EVENT_POST, url, dataMap, callBack);
    }

    static void sendGetRequest(final String url, SimpleApiCallBack callBack) {
        sendApiRequest(EVENT_GET, url, null, callBack);
    }

    static void sendDeleteRequest(final String url, SimpleApiCallBack callBack) {
        sendApiRequest(EVENT_DELETE, url, null, callBack);
    }

    private static void sendApiRequest(String method, final String url, HashMap<String, Object> dataMap, final SimpleApiCallBack callBack) {

        if (SocketManager.getInstance().isConnected()){
            SocketManager.getInstance().sendSocketRequest(method, url, dataMap, new SimpleApiCallBack<Object>() {
                @Override
                public void onApiResponse(final Object result, final Boolean success, final int statusCode) {
                    if (callBack != null){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callBack.onApiResponse(result, success, statusCode);
                            }
                        });
                    }
                }
            });
        } else {
           if (callBack != null){
               callBack.onApiResponse(null, false, 0);
           }
        }
    }
}