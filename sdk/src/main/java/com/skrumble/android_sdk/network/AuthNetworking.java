package com.skrumble.android_sdk.network;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.skrumble.android_sdk.Config;
import com.skrumble.android_sdk.helper.JsonHelper;
import com.skrumble.android_sdk.network.callback.SimpleApiCallBack;
import com.skrumble.android_sdk.network.socket.SocketManager;

import org.json.JSONObject;

import java.util.HashMap;

public class AuthNetworking extends Networking{

    private static String getSignInUrl() { return getAuthServerBaseUrl("/login-user"); }

    public static void login(String email, String password, final SimpleApiCallBack<JSONObject> callBack) {
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("username", email);
        dataMap.put("password", password);
        dataMap.put("grant_type", Config.GRANT_TYPE_PASSWORD);
        dataMap.put("client_id", Config.getClientId());
        dataMap.put("client_secret", Config.getClientSecret());

        String signInUrl = getSignInUrl();

        AndroidNetworking.post(signInUrl)
                .addBodyParameter(dataMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (callBack != null) {
                            callBack.onApiResponse(response, true, 200);
                        }

                        String auth = JsonHelper.getStringFromJSON(response, "access_token");
                        Config.setAuthString(auth);
                        SocketManager.getInstance().registerSocket();
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (callBack != null) {
                            callBack.onApiResponse(null, true, anError.getErrorCode());
                        }
                    }
                });
    }
}