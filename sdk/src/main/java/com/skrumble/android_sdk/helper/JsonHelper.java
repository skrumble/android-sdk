package com.skrumble.android_sdk.helper;

import android.text.TextUtils;

import com.skrumble.android_sdk.model.SKObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class JsonHelper {

    // *********************************************************************************************
    // region Validation

    private static Boolean checkJsonAndKeyEmpty(Object json, String key) {
        if (key == null || key.length() == 0 || json == null) {
            return true;
        }
        if (json instanceof JSONObject && ((JSONObject) json).length() == 0) {
            return true;
        }
        return false;
    }

    // endregion

    // *********************************************************************************************
    // region Int, Long

    public static long getLongFromJSON(Object json, String key) {
        String intString = getStringFromJSON(json, key);
        if (intString.isEmpty()) {
            return -1;
        }
        return Long.parseLong(intString);
    }

    public static int getIntFromJSON(Object json, String key) {
        String intString = getStringFromJSON(json, key);
        if (key == null || key.isEmpty()) {
            return -1;
        }

        return Integer.parseInt(intString);
    }

    // endregion

   // *********************************************************************************************
   // region Date

    public static Date getDateFromJSON(Object object, String key){

        String dateString = getStringFromJSON(object, key);

        return DateHelper.getDateFromServerStringFormat(dateString);
    }

   // endregion

    // *********************************************************************************************
    // region Bool

    public static boolean getBooleanFromJSON(JSONObject object, String key) {
        String value = getStringFromJSON(object, key);

        if (TextUtils.isEmpty(value)) {
            return false;
        }

        return Boolean.parseBoolean(value);
    }

    // endregion

    public static String getStringFromJSON(Object json, String key) {
        try {
            String s = ((JSONObject) json).getString(key);

            if (s.equals("null")) {
                return "";
            } else {
                return s;
            }
        } catch (Exception e) {
            return "";
        }
    }

    // *********************************************************************************************
    // region Object

    public static Object getJsonObjectOrJsonArray(JSONObject json, String key) {
        Object object;
        try {
            object = json.get(key);
        } catch (JSONException e) {
            object = new JSONObject();
        }
        return object;
    }

    public static JSONObject getJsonObjectFromJson(JSONObject json, String key) {
        JSONObject object = null;


        if (checkJsonAndKeyEmpty(json, key)) {
            return new JSONObject();
        }
        try {
            object = json.getJSONObject(key);
        } catch (Exception ignored) {
        }
        if (object == null) {
            object = new JSONObject();
        }

        return object;
    }

    // endregion



    /**
     *  get array value from JSONObject safely
     * *
     * @param json json object
     * @param key json key
     * @return Array or [] when json value is null
     * @exception JSONException throw when get json value exception occurs
     */
    public static <T> ArrayList<T> getObjectArrayFromJSON(JSONObject json, String key, Class<T> clazz) {
        try {
            if (!json.has(key)){
                return new ArrayList<>(0);
            }

            JSONArray jsonArray = json.getJSONArray(key);
            int i = 0;
            ArrayList<T> array = new ArrayList<>();

            for (i = 0; i < jsonArray.length(); i++) {
                T instance = clazz.newInstance();
                if (instance instanceof SKObject) {
                    Constructor constructor = clazz.getConstructor(JSONObject.class);
                    array.add((T) constructor.newInstance(jsonArray.get(i)));
                } else {
                    array.add((T) jsonArray.get(i));
                }
            }
            return array;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static <T extends SKObject> HashMap<String, T> getObjectHasMapFromJSON(JSONObject object, String key, Class<T> clazz) {

        if (object == null || object.has(key) == false) {
            return new HashMap<>();
        }

        try {

            HashMap<String, T> result = new HashMap<>();

            JSONArray jsonArray = object.getJSONArray(key);

            for (int i = 0; i < jsonArray.length(); i++) {
                T instance = clazz.newInstance();

                if (instance != null) {
                    Constructor<T> constructor = clazz.getConstructor(JSONObject.class);
                    T obj = constructor.newInstance(jsonArray.get(i));
                    result.put(obj.getId(), obj);
                }
            }

            return result;

        } catch (Exception e) {
            return new HashMap<>();
        }
    }
}