package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.enumeration.Role;
import com.skrumble.android_sdk.enumeration.Theme;
import com.skrumble.android_sdk.enumeration.UserState;
import com.skrumble.android_sdk.enumeration.UserStatus;
import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class TeamUser extends Person {

    private Role role;
    private UserStatus status;
    private UserState state;
    private Theme theme;

    private GeoLocation geoLocation;

    private ArrayList<String> extension;

    private Date lastLogin;
    private Date deleteAt;
    private Date accepted;

    private String position;
    private String email;
    private String workNumber;
    private String homeNumber;
    private String mobileNumber;
    private String website;
    private String forwardNumber;
    private String thumbUrl;
    private String teamUserId;
    private String team;
    private String user;

    private boolean forward;
    private boolean notification;

    // *********************************************************************************************
    // region Constructor

    public TeamUser(JSONObject object) {
        role = Role.getRole(JsonHelper.getStringFromJSON(object, "role"));
        status = UserStatus.fromString(JsonHelper.getStringFromJSON(object, "status"));
        state = UserState.fromString(JsonHelper.getStringFromJSON(object, "state"));
        theme = Theme.getTheme(JsonHelper.getStringFromJSON(object, "theme"));

        geoLocation = new GeoLocation(object);

        extension = JsonHelper.getObjectArrayFromJSON(object, "extension", String.class);

        lastLogin = JsonHelper.getDateFromJSON(object, "last_login");
        deleteAt = JsonHelper.getDateFromJSON(object, "deleted_at");
        accepted = JsonHelper.getDateFromJSON(object, "accepted");

        position = JsonHelper.getStringFromJSON(object, "position");
        email = JsonHelper.getStringFromJSON(object, "email");
        workNumber = JsonHelper.getStringFromJSON(object, "work_number");
        homeNumber = JsonHelper.getStringFromJSON(object, "home_number");
        mobileNumber = JsonHelper.getStringFromJSON(object, "mobile_number");
        website = JsonHelper.getStringFromJSON(object, "website");
        forwardNumber = JsonHelper.getStringFromJSON(object, "forward_number");
        thumbUrl = JsonHelper.getStringFromJSON(object, "thumb_url");
        teamUserId = JsonHelper.getStringFromJSON(object, "teamUserId");
        team = JsonHelper.getStringFromJSON(object, "team");
        user = JsonHelper.getStringFromJSON(object, "user");

        forward = JsonHelper.getBooleanFromJSON(object, "forward");
        notification = JsonHelper.getBooleanFromJSON(object, "notification");
    }

    // endregion

    // *********************************************************************************************
    // region Factory

    public static ArrayList<TeamUser> teamUserListFactory(JSONArray jsonArray) {
        ArrayList<TeamUser> teamUsers = new ArrayList<>();

        if (jsonArray == null || jsonArray.length() == 0){
            return teamUsers;
        }

        int size = jsonArray.length();

        for (int i = 0; i < size; i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = (JSONObject) jsonArray.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject != null){
                TeamUser teamUser = new TeamUser(jsonObject);
                teamUsers.add(teamUser);
            }
        }

        return teamUsers;
    }
    // endregion

    // *********************************************************************************************
    // region Setter Getter

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public UserState getState() {
        return state;
    }

    public void setState(UserState state) {
        this.state = state;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<String> getExtension() {
        return extension;
    }

    public void setExtension(ArrayList<String> extension) {
        this.extension = extension;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getDeleteAt() {
        return deleteAt;
    }

    public void setDeleteAt(Date deleteAt) {
        this.deleteAt = deleteAt;
    }

    public Date getAccepted() {
        return accepted;
    }

    public void setAccepted(Date accepted) {
        this.accepted = accepted;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getForwardNumber() {
        return forwardNumber;
    }

    public void setForwardNumber(String forwardNumber) {
        this.forwardNumber = forwardNumber;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getTeamUserId() {
        return teamUserId;
    }

    public void setTeamUserId(String teamUserId) {
        this.teamUserId = teamUserId;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isForward() {
        return forward;
    }

    public void setForward(boolean forward) {
        this.forward = forward;
    }

    public boolean isNotification() {
        return notification;
    }

    public void setNotification(boolean notification) {
        this.notification = notification;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.role == null ? -1 : this.role.ordinal());
        dest.writeInt(this.status == null ? -1 : this.status.ordinal());
        dest.writeInt(this.state == null ? -1 : this.state.ordinal());
        dest.writeInt(this.theme == null ? -1 : this.theme.ordinal());

        dest.writeParcelable(this.geoLocation, flags);

        dest.writeStringList(this.extension);

        dest.writeLong(this.lastLogin != null ? this.lastLogin.getTime() : -1);
        dest.writeLong(this.deleteAt != null ? this.deleteAt.getTime() : -1);
        dest.writeLong(this.accepted != null ? this.accepted.getTime() : -1);

        dest.writeString(this.position);
        dest.writeString(this.email);
        dest.writeString(this.workNumber);
        dest.writeString(this.homeNumber);
        dest.writeString(this.mobileNumber);
        dest.writeString(this.website);
        dest.writeString(this.forwardNumber);
        dest.writeString(this.thumbUrl);
        dest.writeString(this.teamUserId);
        dest.writeString(this.team);
        dest.writeString(this.user);
        dest.writeByte(this.forward ? (byte) 1 : (byte) 0);
        dest.writeByte(this.notification ? (byte) 1 : (byte) 0);
    }

    private TeamUser(Parcel in) {
        super(in);

        int tmpRole = in.readInt();
        this.role = tmpRole == -1 ? null : Role.values()[tmpRole];
        int tmpStatus = in.readInt();
        this.status = tmpStatus == -1 ? null : UserStatus.values()[tmpStatus];
        int tmpState = in.readInt();
        this.state = tmpState == -1 ? null : UserState.values()[tmpState];
        int tmpTheme = in.readInt();
        this.theme = tmpTheme == -1 ? null : Theme.values()[tmpTheme];

        this.geoLocation = in.readParcelable(GeoLocation.class.getClassLoader());

        this.extension = in.createStringArrayList();
        long tmpLastLogin = in.readLong();

        this.lastLogin = tmpLastLogin == -1 ? null : new Date(tmpLastLogin);
        long tmpDeleteAt = in.readLong();
        this.deleteAt = tmpDeleteAt == -1 ? null : new Date(tmpDeleteAt);
        long tmpAccepeted = in.readLong();
        this.accepted = tmpAccepeted == -1 ? null : new Date(tmpAccepeted);

        this.position = in.readString();
        this.email = in.readString();
        this.workNumber = in.readString();
        this.homeNumber = in.readString();
        this.mobileNumber = in.readString();
        this.website = in.readString();
        this.forwardNumber = in.readString();
        this.thumbUrl = in.readString();
        this.teamUserId = in.readString();
        this.team = in.readString();
        this.user = in.readString();

        this.forward = in.readByte() != 0;
        this.notification = in.readByte() != 0;
    }

    public static final Parcelable.Creator<TeamUser> CREATOR = new Parcelable.Creator<TeamUser>() {
        @Override
        public TeamUser createFromParcel(Parcel source) {
            return new TeamUser(source);
        }

        @Override
        public TeamUser[] newArray(int size) {
            return new TeamUser[size];
        }
    };

    // endregion
}