package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

public class GeoLocation implements Parcelable {
    private long latitude;
    private long longitude;

    // *********************************************************************************************
    // region Constructor

    public GeoLocation(JSONObject object){
        latitude = JsonHelper.getLongFromJSON(object, "latitude");
        longitude = JsonHelper.getLongFromJSON(object, "longitude");
    }

    // endregion

    // *********************************************************************************************
    // region Setter Getter

    public long getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.latitude);
        dest.writeLong(this.longitude);
    }

    private GeoLocation(Parcel in) {
        this.latitude = in.readLong();
        this.longitude = in.readLong();
    }

    public static final Parcelable.Creator<GeoLocation> CREATOR = new Parcelable.Creator<GeoLocation>() {
        @Override
        public GeoLocation createFromParcel(Parcel source) {
            return new GeoLocation(source);
        }

        @Override
        public GeoLocation[] newArray(int size) {
            return new GeoLocation[size];
        }
    };

    // endregion
}