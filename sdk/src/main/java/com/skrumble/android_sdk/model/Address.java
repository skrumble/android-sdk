package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

public class Address implements Parcelable {

    public String city;
    public String country;
    public String postal;
    public String state;
    public String address1;
    public String address2;

    // *********************************************************************************************
    // region Constructor

    public Address(JSONObject object){
        city = JsonHelper.getStringFromJSON(object, "city");
        country = JsonHelper.getStringFromJSON(object, "country");
        postal = JsonHelper.getStringFromJSON(object, "postal");
        state = JsonHelper.getStringFromJSON(object, "state");
        address1 = JsonHelper.getStringFromJSON(object, "address1");
        address2 = JsonHelper.getStringFromJSON(object, "address2");
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.city);
        dest.writeString(this.country);
        dest.writeString(this.postal);
        dest.writeString(this.state);
        dest.writeString(this.address1);
        dest.writeString(this.address2);
    }

    protected Address(Parcel in) {
        this.city = in.readString();
        this.country = in.readString();
        this.postal = in.readString();
        this.state = in.readString();
        this.address1 = in.readString();
        this.address2 = in.readString();
    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    // endregion

}