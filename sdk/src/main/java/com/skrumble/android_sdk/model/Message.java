package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.enumeration.DeliveryStatus;
import com.skrumble.android_sdk.enumeration.MessageType;
import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class Message extends SKObject {

    private TeamUser senderUser;

    private MessageType messageType = MessageType.None;

    private DeliveryStatus deliveryStatus = DeliveryStatus.NotDelivered;

    private Date createAt;

    private String chatId;
    private String pinned;
    private String unread;
    private String language;
    private String thread;

    private int threadCount = 0;

    private boolean thread_subscribed = false;
    private boolean edited = false;

    //Text
    private String body;

    //File
    private SKFile skFile;

    // *********************************************************************************************
    // region Constructor

    public Message(JSONObject jsonObject) {
        super(jsonObject);

        JSONObject from = JsonHelper.getJsonObjectFromJson(jsonObject, "from");
        senderUser = new TeamUser(from);

        messageType = MessageType.fromString(JsonHelper.getStringFromJSON(jsonObject, "type"));

        deliveryStatus = DeliveryStatus.Delivered;

        createAt = JsonHelper.getDateFromJSON(jsonObject, "created_at");

        chatId = JsonHelper.getStringFromJSON(jsonObject, "chat");
        pinned = JsonHelper.getStringFromJSON(jsonObject, "pinned");
        unread = JsonHelper.getStringFromJSON(jsonObject, "unread");
        language = JsonHelper.getStringFromJSON(jsonObject, "language");
        thread = JsonHelper.getStringFromJSON(jsonObject, "thread");

        threadCount = JsonHelper.getIntFromJSON(jsonObject, "thread_count");

        thread_subscribed = JsonHelper.getBooleanFromJSON(jsonObject, "thread_subscribed");

        edited = JsonHelper.getBooleanFromJSON(jsonObject, "edited");

        //Text Message
        body = JsonHelper.getStringFromJSON(jsonObject, "body");

        //file
        skFile = new SKFile(JsonHelper.getJsonObjectFromJson(jsonObject, "file"));
    }

    // endregion

    // *********************************************************************************************
    // region Factory

    public static ArrayList<Message> messageListFactory(JSONArray jsonArray){
        ArrayList<Message> messageArrayList = new ArrayList<>();

        if (jsonArray == null || jsonArray.length() == 0) {
            return messageArrayList;
        }

        int size = jsonArray.length();

        for (int i = 0; i < size; i++) {

            JSONObject jsonObject = null;
            try {
                jsonObject = (JSONObject) jsonArray.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject != null){
                Message message = new Message(jsonObject);
                if (message.isActiveSKObject()){
                    messageArrayList.add(message);
                }
            }
        }

        return messageArrayList;
    }

    // endregion

    // *********************************************************************************************
    // region Utility

    @Override
    public boolean isActiveSKObject() {
        return messageType == MessageType.Text || messageType == MessageType.File;
    }


    // endregion

   // *********************************************************************************************
   // region Setter Getter

    public TeamUser getSenderUser() {
        return senderUser;
    }

    public void setSenderUser(TeamUser senderUser) {
        this.senderUser = senderUser;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getPinned() {
        return pinned;
    }

    public void setPinned(String pinned) {
        this.pinned = pinned;
    }

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getThread() {
        return thread;
    }

    public void setThread(String thread) {
        this.thread = thread;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public boolean isThread_subscribed() {
        return thread_subscribed;
    }

    public void setThread_subscribed(boolean thread_subscribed) {
        this.thread_subscribed = thread_subscribed;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public SKFile getSkFile() {
        return skFile;
    }

    public void setSkFile(SKFile skFile) {
        this.skFile = skFile;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeParcelable(this.senderUser, flags);
        dest.writeInt(this.messageType == null ? -1 : this.messageType.ordinal());
        dest.writeInt(this.deliveryStatus == null ? -1 : this.deliveryStatus.ordinal());
        dest.writeLong(this.createAt != null ? this.createAt.getTime() : -1);
        dest.writeString(this.chatId);
        dest.writeString(this.pinned);
        dest.writeString(this.unread);
        dest.writeString(this.language);
        dest.writeString(this.thread);
        dest.writeInt(this.threadCount);
        dest.writeByte(this.thread_subscribed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.edited ? (byte) 1 : (byte) 0);

        dest.writeString(body);

        dest.writeParcelable(skFile, flags);
    }

    protected Message(Parcel in) {
        super(in);

        this.senderUser = in.readParcelable(TeamUser.class.getClassLoader());
        int tmpMessageType = in.readInt();
        this.messageType = tmpMessageType == -1 ? null : MessageType.values()[tmpMessageType];
        int tmpDeliveryStatus = in.readInt();
        this.deliveryStatus = tmpDeliveryStatus == -1 ? null : DeliveryStatus.values()[tmpDeliveryStatus];
        long tmpCreateAt = in.readLong();
        this.createAt = tmpCreateAt == -1 ? null : new Date(tmpCreateAt);
        this.chatId = in.readString();
        this.pinned = in.readString();
        this.unread = in.readString();
        this.language = in.readString();
        this.thread = in.readString();
        this.threadCount = in.readInt();
        this.thread_subscribed = in.readByte() != 0;
        this.edited = in.readByte() != 0;

        this.body = in.readString();

        this.skFile = in.readParcelable(SKFile.class.getClassLoader());
    }

    public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    // endregion
}
