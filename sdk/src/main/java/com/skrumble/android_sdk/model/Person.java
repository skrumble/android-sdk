package com.skrumble.android_sdk.model;

import android.os.Parcel;

import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

import java.util.Date;

/**
 * It is base class of User, Team User.
 * This object contain base information related to person like name, avatar, color, and created date.
 */
public class Person extends SKObject {

    private String firstName;
    private String lastName;
    private String color;
    private String avatar;

    private Date createdAt;

    // *********************************************************************************************
    // region Constructor

    /**
     * Emmpty constructor to initialize members.
     */
    public Person(){
        super();
        firstName = "";
        lastName = "";
        color = "";
        avatar = "";
        createdAt = new Date();
    }

    /**
     * Constructor : Create person object from json
     * @param object Type: JSONObject - pass object to get fields.
     */
    public Person(JSONObject object){
        super(object);
        firstName = JsonHelper.getStringFromJSON(object, "first_name");
        lastName = JsonHelper.getStringFromJSON(object, "last_name");
        avatar = JsonHelper.getStringFromJSON(object, "avatar");
        color = JsonHelper.getStringFromJSON(object, "color");
        createdAt = JsonHelper.getDateFromJSON(object, "created_at");
    }

    // endregion

    // *********************************************************************************************
    // region Setter and Getter

    /**
     * Get First name of Person
     * @return Type: String
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set First name of person
     * @param firstName Type: String - string to set person's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get Last name to person
     * @return Type: String
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set Last name to person
     * @param lastName Tpye: String - string to set person's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get color for avatar background
     * @return Type: String
     */
    public String getColor() {
        return color;
    }

    /**
     * Set color to person
     * @param color Type: String - string to set color to person object
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Get avatar URL of person
     * @return Type: String
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Set avatar URL to person object
     * @param avatar Type: String
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * Get date when this object is created.
     * @return Type: Date
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Set created at date of object (This will never use.)
     * @param createdAt Type : Date
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    // endregion

    // *********************************************************************************************
    // region Parcel

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out,flags);
        out.writeString(firstName);
        out.writeString(lastName);
        out.writeString(color);
        out.writeString(avatar);
        out.writeLong(createdAt.getTime());
    }

    public Person(Parcel in){
        super(in);
        firstName = in.readString();
        lastName = in.readString();
        color = in.readString();
        avatar = in.readString();
        createdAt = new Date(in.readLong());
    }

    // endregion
}
