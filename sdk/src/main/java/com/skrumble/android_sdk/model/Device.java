package com.skrumble.android_sdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.skrumble.android_sdk.helper.JsonHelper;

import org.json.JSONObject;

public class Device extends SKObject {

    private String registrationID;
    private String type;
    private boolean debug;
    private boolean doNotDisturb;
    private String uuid;
    private int unseenNotification;

    // *********************************************************************************************
    // region Constructor

    // endregion

    /**
     * Device default constructor
     */
    public Device(){

    }

    public Device(JSONObject object){
        super(object);

        registrationID = JsonHelper.getStringFromJSON(object, "registrationId");
        type = JsonHelper.getStringFromJSON(object, "type");

        debug = JsonHelper.getBooleanFromJSON(object, "debug");
        doNotDisturb = JsonHelper.getBooleanFromJSON(object, "doNotDisturb");

        uuid = JsonHelper.getStringFromJSON(object, "uuid");

        unseenNotification = JsonHelper.getIntFromJSON(object, "unseenNotification");
    }

    // *********************************************************************************************
    // region Setter Getter

    public String getRegistrationID() {
        return registrationID;
    }

    public void setRegistrationID(String registrationID) {
        this.registrationID = registrationID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public boolean isDoNotDisturb() {
        return doNotDisturb;
    }

    public void setDoNotDisturb(boolean doNotDisturb) {
        this.doNotDisturb = doNotDisturb;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getUnseenNotification() {
        return unseenNotification;
    }

    public void setUnseenNotification(int unseenNotification) {
        this.unseenNotification = unseenNotification;
    }


    // endregion

    // *********************************************************************************************
    // region Parcel

    public static final Parcelable.Creator<Device> CREATOR = new Parcelable.Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel parcel) {
            return new Device(parcel);
        }

        @Override
        public Device[] newArray(int i) {
            return new Device[i];
        }
    };

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);

        out.writeString(registrationID);
        out.writeString(type);
        out.writeByte((byte) (debug ? 1 :0));
        out.writeByte((byte) (doNotDisturb ? 1 :0));
        out.writeString(uuid);
        out.writeInt(unseenNotification);
    }

    public Device(Parcel parcel){
        super(parcel);
        registrationID = parcel.readString();
        type = parcel.readString();
        debug = parcel.readByte() != 0;
        doNotDisturb = parcel.readByte() != 0;
        uuid = parcel.readString();
        unseenNotification = parcel.readInt();
    }

    // endregion
}