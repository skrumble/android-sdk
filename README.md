# SkrumbleSDK - JAVA

### Installation

#### Gradle
Step 1. Add it in your root build.gradle at the end of repositories:
```java
allprojects {
	repositories {
	...
	maven { url 'https://jitpack.io' }
	}
}
```
Step 2. Add the dependency
```java
dependencies {
	    implementation 'com.gitlab.skrumble:android-sdk:0.0.1'
	}
```	

#### Maven

Step 1. Add the JitPack repository to your build file

```java
<repositories>
	<repository>
		<id>jitpack.io</id>
		<url>https://jitpack.io</url>
	</repository>
</repositories>
```

Step 2. Add the dependency
```java
<dependency>
    <groupId>com.gitlab.skrumble</groupId>
    <artifactId>android-sdk</artifactId>
    <version>-SNAPSHOT</version>
</dependency>
```


### Configuration

```java
Call following statement in oncreate of application class

SkrumbleSDK.ConfigApi("auth_server", "api_server", "client_id","client_secret", "socket_server");
SkrumbleSDK.getInstance().start();
```

#### Login
```java
AuthNetworking.login("email", "password", callback)
```    

#### Chat
```java
ChatNetworking.getAllChat(callback)
```    
### Roadmap
The goal of the SDK is to provide convenient access to the full functionality of the [Skrumble REST API](http://developers.skrumble.com/knowledge-base/skrumble-rest-api/). For maintainers/contributors, unchecked items below are unimplemented, and therefore a good place to start contributing. **Bold items** are high-priority features. See our list of issues for information about individual features.

- [ ] Teams
    - [X] Create team
    - [ ] Update team info
- [ ] Users
    - [X] Create user
    - [ ] Add to team
    - [ ] Update user info
    - [ ] Invite user
    - [ ] Invite guest
    - [ ] Get one
    - [X] Get all
    - [ ] Check existing
    - [ ] User login
    - [ ] Guest login
    - [ ] Deactivate user
    - [ ] Register device for notification
    - [ ] Deregister device for notification
- [ ] Chat
    - [X] Create
    - [X] Get one
    - [X] Get all
    - [X] Update chat info
    - [X] Delete chat
    - [ ] Generate guest url
    - [ ] Mark as read
    - [ ] Add user to group
    - [ ] Remove user from group
    - [ ] Messages
        - [X] Send Text Message
        - [ ] Send File Message
        - [X] Get messages list
        - [ ] Get unread
        - [ ] Translate message
    - [ ] Links
        - [ ] Get links by chat
        - [ ] Get links for user
    - [ ] Files
        - [ ] Get files by chat
        - [ ] Get files for user
        - [ ] Get file info 
- [ ] Integrations
    - [ ] Integration type support:
        - [ ] Google
        - [ ] Office365
        - [ ] Exchange
    - [ ] Create integration
    - [ ] Update integration
    - [ ] Delete integration
    - [ ] Contacts
        - [ ] Create contact
        - [ ] Update contact
        - [ ] Delete contact
        - [ ] Get one 
        - [ ] Get all
    - [ ] Events
        - [ ] Add event
        - [ ] Update event
        - [ ] Delete event 
        - [ ] Get all 
        - [ ] Get one
- [ ] Billing
    - [ ] Add funds
    - [ ] Get overview
    - [ ] Get subscriptions
    - [ ] Add billing address
    - [ ] Get draft invoice PDF
    - [ ] Get invoice PDF
    - [ ] Credit Cards
        - [ ] Add card
        - [ ] Delete card 
        - [ ] Get all
- [ ] Socket
    - [ ] Chat
    - [ ] Message
    - [ ] Current User
    - [ ] Team User
 